# WhallaCryptoiOS

[![CI Status](http://img.shields.io/travis/Marcin Gorny/WhallaCryptoiOS.svg?style=flat)](https://travis-ci.org/Marcin Gorny/WhallaCryptoiOS)
[![Version](https://img.shields.io/cocoapods/v/WhallaCryptoiOS.svg?style=flat)](http://cocoadocs.org/docsets/WhallaCryptoiOS)
[![License](https://img.shields.io/cocoapods/l/WhallaCryptoiOS.svg?style=flat)](http://cocoadocs.org/docsets/WhallaCryptoiOS)
[![Platform](https://img.shields.io/cocoapods/p/WhallaCryptoiOS.svg?style=flat)](http://cocoadocs.org/docsets/WhallaCryptoiOS)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WhallaCryptoiOS is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "WhallaCryptoiOS" , :git => 'git@bitbucket.org:whalla/whallacryptoios.git'

## Author

Marcin Gorny, marcin.gorny@gmail.com

## License

WhallaCryptoiOS is available under the MIT license. See the LICENSE file for more info.

