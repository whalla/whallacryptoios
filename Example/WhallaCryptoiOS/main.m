//
//  main.m
//  WhallaCryptoiOS
//
//  Created by Marcin Gorny on 12/17/2014.
//  Copyright (c) 2014 Marcin Gorny. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WLAppDelegate class]));
    }
}
